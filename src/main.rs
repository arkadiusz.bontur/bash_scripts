use std::{io::{stdout, Write, Stdout} };
use crossterm::{
    ExecutableCommand, QueueableCommand,
    terminal::{self, enable_raw_mode, disable_raw_mode,}, cursor, style::{self, Stylize}, Result,
    event::{poll, read, Event, KeyCode},
};
use std::{thread, time};

const N_SCREEN_WIDTH: isize = 80;			// Console Screen Size X (columns)
const N_SCREEN_HEIGHT: isize = 30;			// Console Screen Size Y (rows)
const N_FIELD_WIDTH: isize = 12;
const N_FIELD_HEIGHT: isize = 18;

// const TETRIS:          x . y . z  
const TETROMINO: [[[char; 4]; 4]; 3] = [
    [
        ['X',' ',' ',' '],
        ['X',' ',' ',' '],
        ['X',' ',' ',' '],
        ['X',' ',' ',' '],
    ],
    [
        ['X',' ',' ',' '],
        ['X',' ',' ',' '],
        ['X','X',' ',' '],
        [' ',' ',' ',' '],
    ],
    [//x  0   1   2   3
        ['X','X','X','.'], // y = 0
        ['.','X','.','.'], // 1
        ['.','.','.','.'], // 2
        ['.','.','.','.'], // 3
    ],

];
struct Vector2D {x: f64, y: f64}
struct Point2D {x: f64, y: f64}

fn welcome_text(screen: &mut [[char; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize]) {
    let welcome = "WELCOME IN TETRIS".chars();
    //    ["_____ _____ _____ ____  ___ ____  ".chars()],
    //    ["|_   _| ____|_   _|  _ \\|_ _/ ___| ".chars()],
    //    ["  | | |  _|   | | | |_) || |\\___ \\ ".chars()],
    //    ["  | | | |___  | | |  _ < | | ___) |".chars()],
    //    ["  |_| |_____| |_| |_| \\_\\___|____/ ".chars()],
    //];
    for (xi, x) in welcome.enumerate() {
            screen[xi + 30][1] = x;
    }
}

fn setup_field(screen: &mut [[char; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize]){
    let mut field = [[' '; N_FIELD_HEIGHT as usize]; N_FIELD_WIDTH as usize];
    for (yi, y) in field.iter_mut().enumerate() {
        for (xi, x) in y.iter_mut().enumerate() {
            if yi == 0 || yi == N_FIELD_WIDTH as usize -1 {*x = '#'}
            if xi == N_FIELD_HEIGHT as usize -1 {*x = '#'}
        }
    }
    for (yi, y) in field.iter().enumerate() {
        for (xi, x) in y.iter().enumerate() {
            screen[yi + 1][xi + 1] = *x;
        }
    }
}

fn debug_info(screen: &mut [[char; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize], rotation: isize, location: &Point2D) {
    let mut info = Vec::new();
    info.push(format!("| rotation: {}", rotation ));
    info.push(format!("| location: ({}, {})", location.x, location.y ));

    for (yi, y) in info.iter().enumerate() {
        for (xi, x) in y.chars().enumerate() {
            screen[xi + N_FIELD_WIDTH as usize + 2][yi + 2] = x;
        }
    }
}

fn does_piece_fit(screen: &[[char; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize], piece_number: usize, rotation: isize, position: &Point2D) -> bool {
    for (yi, y) in TETROMINO[piece_number].iter().enumerate() {
        for (xi, x) in y.iter().enumerate() {
            let new_position = rotate_cw(rotation, position);
            if screen[xi + new_position.x as usize][yi + new_position.y as usize] == '#' && *x == 'X' {
                return false;
            }
        }
    }
    return true;
}

fn add_vec(point: &Point2D, vec: &Vector2D) -> Point2D {
    Point2D {
        x: point.x + vec.x,
        y: point.y + vec.y,
    }
}
fn sub_vec(point: &Point2D, vec: &Vector2D) -> Point2D {
    Point2D {
        x: point.x - vec.x,
        y: point.y - vec.y,
    }
}

// Counter clock wise
fn rotate_ccw(angle: usize, point: &Point2D) -> Point2D {
    let vec = Vector2D { x: 1.5, y: 1.5 };
    let point = sub_vec( point, &vec );
    match angle {
        0 => add_vec(&Point2D { x: point.x, y: point.y }, &vec),
        1 => add_vec(&Point2D { x: - point.y, y: point.y }, &vec),
        2 => add_vec(&Point2D { x: - point.x, y: - point.y }, &vec),
        3 => add_vec(&Point2D { x: point.y, y: - point.x }, &vec), 
        _ => add_vec(&Point2D { x: point.x, y: point.y }, &vec),
    }
}

// Counter clock wise
fn rotate_cw(angle: isize, point: &Point2D) -> Point2D {
    let vec = Vector2D { x: 1.5, y: 1.5 };
    let point = sub_vec(point, &vec);
    match angle {
        0 => add_vec(&Point2D { x: point.x, y: point.y }, &vec),     // 0   degrees
        1 => add_vec(&Point2D { x: point.y, y: - point.x }, &vec),   // 90  degrees
        2 => add_vec(&Point2D { x: - point.x, y: - point.y }, &vec), // 180 degrees
        3 => add_vec(&Point2D { x: - point.y, y: point.x}, &vec),   // 270 degrees
        _ => add_vec(&Point2D { x: point.x, y: point.y }, &vec),     // 0   degrees
    }
}

//fn rotate(angle: isize, point: & Point2D) -> Point2D {
//    if angle >= 0 {
//        rotate_cw(angle, point)
//    } else {
//        rotate_ccw(angle.abs() as usize, point)
//    }
//}

fn draw_piece(screen: &mut [[char; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize], rotation: isize, piece_number: usize, point: & Point2D) {
    for (yi, y) in TETROMINO[piece_number].iter().enumerate() {
        for (xi, _x) in y.iter().enumerate() {
            let rotated: Point2D = if rotation >= 0 {
                rotate_cw(rotation, &Point2D { x: xi as f64, y: yi as f64})
            } else {
                rotate_ccw(rotation.abs() as usize, &Point2D { x: xi as f64, y: yi as f64})
            };
            let char: char = TETROMINO[piece_number][rotated.y as usize][rotated.x as usize];
            if char != ' ' {
                screen[xi + point.x as usize][yi + point.y as usize] = char;
            }

        }
    }
}

fn print_screen(stdout: &mut Stdout, screen: &mut [[char; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize]) -> Result<()> {
    for (yi, y) in screen.iter().enumerate(){
        for (xi, x) in y.iter().enumerate(){
            let mut pixel = *x;
            if yi == 0 || yi == N_SCREEN_WIDTH as usize -1 {pixel = '|' }
            if xi == 0 || xi == N_SCREEN_HEIGHT as usize -1 {pixel = '-' }
            if xi == 0 && yi == 0 {pixel = '/'}
            if xi == N_SCREEN_WIDTH as usize -1 && yi == N_SCREEN_HEIGHT as usize - 1 { pixel = '/' }
            if xi == N_SCREEN_WIDTH as usize -1 && yi == 0 {pixel = '\\'}
            stdout.queue(cursor::MoveTo(yi as u16, xi as u16))?
                .queue(style::Print(pixel.red()))?;
        }
    }
    stdout.queue(cursor::MoveTo(0,0))?
        .queue(style::Print('/'.red()))?;
    stdout.queue(cursor::MoveTo(N_SCREEN_WIDTH as u16 -1,N_SCREEN_HEIGHT as u16 -1))?
        .queue(style::Print('/'.red()))?;
    stdout.queue(cursor::MoveTo(N_SCREEN_WIDTH as u16 -1,0))?
        .queue(style::Print('\\'.red()))?;
    stdout.queue(cursor::MoveTo(0, N_SCREEN_HEIGHT as u16 -1))?
        .queue(style::Print('\\'.red()))?;
    stdout.flush()?;
    Ok(())
}

fn main() -> Result<()> {
    enable_raw_mode()?;
    let mut stdout = stdout();
    stdout.execute(terminal::EnterAlternateScreen)?;
    stdout.execute(terminal::SetTitle("Tetris"))?;
    stdout.execute(terminal::SetSize(100, 100))?;

    let mut screen = [[' '; N_SCREEN_HEIGHT as usize]; N_SCREEN_WIDTH as usize];

    welcome_text(&mut screen);
    let _score: usize = 0;
    let _game_over: bool = false;
    let current_piece: usize= 1;
    let mut piece_location: Point2D = Point2D { x: N_FIELD_WIDTH as f64 /2., y: 1. };
    let mut rotation: isize= 0;
    loop{
        // Timing =======================
        thread::sleep(time::Duration::from_millis(25));
        // Input =======================
        if poll(time::Duration::from_millis(25))? {
            let event = read()?;
            if event == Event::Key(KeyCode::Right.into()){
                let new_location = add_vec(&piece_location, &Vector2D { x: 1., y: 0. });
                if does_piece_fit(&screen, current_piece, rotation, &new_location) {
                    piece_location = new_location;
                }
            }
            if event == Event::Key(KeyCode::Left.into()){
                let new_location = add_vec(&piece_location, &Vector2D { x: -1., y: 0. });
                if does_piece_fit(&screen, current_piece, rotation, &new_location) {
                    piece_location = new_location;
                }
            }
            if event == Event::Key(KeyCode::Up.into()){
                let tmp_rotation = (rotation + 1) % 4;
                if does_piece_fit(&screen, current_piece, tmp_rotation, &piece_location){
                    rotation = tmp_rotation;
                }
                rotation = tmp_rotation;
            }
            if event == Event::Key(KeyCode::Down.into()){
                rotation = (rotation - 1) % 4;
            }
            if event == Event::Key(KeyCode::Char('q').into()) {
                disable_raw_mode()?;
                break;
            }
        }
        // piece_location = add_vec(&piece_location, &Vector2D { x: 0., y: 1. });
        // Collect and generate frame
        setup_field(&mut screen);
        draw_piece(&mut screen, rotation, current_piece, &piece_location);
        debug_info(&mut screen, rotation, &piece_location);
        
        // Draw the frame on the screen
        print_screen(&mut stdout, &mut screen)?;
    }
    stdout.execute(terminal::LeaveAlternateScreen)?;
    disable_raw_mode()?;
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_rotate0() {
        assert_eq!(rotate_cw(0, &Point2D{ x: 0., y: 0. }).x, 0.);
        assert_eq!(rotate_ccw(0, &Point2D{ x: 0., y: 0. }).x, 0.);
    }
    #[test]
    fn print_test() {
    }
    #[test]
    fn test_rotate1() {
        let p1 = Point2D{x: 0., y: 0.};
        let p_expected = Point2D{x: 0., y: 3.};
        let p_actual = rotate_cw(1, &p1);
        assert_eq!(p_expected.x, p_actual.x);
        assert_eq!(p_expected.y, p_actual.y);
    }
    #[test]
    fn test_rotate3() {
        let mut p1 = Point2D{x: 0., y: 0.};
        let mut p_expected = Point2D{x: 3., y: 0.};
        let mut p_actual = rotate_cw(3, &p1);
        assert_eq!(p_actual.x, p_expected.x);
        assert_eq!(p_actual.y, p_expected.y);

        p1 = Point2D{x: 3., y: 3.};
        p_expected = Point2D{x: 0., y: 3.};
        p_actual = rotate_cw(3, &p1);
        assert_eq!(p_actual.x, p_expected.x);
        assert_eq!(p_actual.y, p_expected.y);
    }
}